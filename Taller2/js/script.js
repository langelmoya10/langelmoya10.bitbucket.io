function nuevoTweet(autor, descripcion, enlace, fecha) {
   var img=  $("<img/>",{
     "class": "img-tweet ",
     html:enlace
   }).attr("src","img/twitter.jpg");

   var title = $("<h5/>", {
      "class":"tweet-autor ",
      html: autor
    })

    var p1 = $("<p/>",{
      "class": "tweet-descripcion",
      html: descripcion
    })

    var a=$("<a/>", {
      "class": "tweet-enlace",
      html:enlace
    })

    var p2 = $("<p/>",{
      "class": "tweet-fecha",
      html: fecha
    })

    var divImg= $( "<div/>", { //imagen
      "class": "tweet-img col-3 col-lg-3 d-none d-md-block"
    });

    var divCuerpo= $( "<div/>", {
      "class": "tweet-body col-9 col-lg-9"
    });

    var p3 = $("<p/>",{
      "class": "tweet-fin",
      html: "-------------------------------------------------"
    })


    img.appendTo(divImg)
    title.appendTo(divCuerpo)
    p1.appendTo(divCuerpo)
    a.appendTo(divCuerpo)
    p2.appendTo(divCuerpo)
    p3.appendTo(divCuerpo)
    divImg.appendTo("#tweets");
    divCuerpo.appendTo("#tweets");
}

function valores() {
    var texto = $("input#texto").val();
    var urlTweet="https://twitrss.me/twitter_search_to_rss/?term="+texto;
    //var listaCuadro=document.getELementByTagName("cuadro");
    //cuadro[0].innerHTML()
    $(".cuadrotexto").text(texto);

    $.ajax({
      url:urlTweet,
      type:"GET",
      dataType:"xml",

      success:function(xml){
        $(xml).find('item').each(function(){
          var autor = $(this).find('dc\\:creator, creator').text();
          var descripcion = $(this).find('description').text();
          var enlace = $(this).find('guid').text();
          var fecha = $(this).find('pubDate').text();
          nuevoTweet(autor, descripcion, enlace, fecha);
         });
      },
      error:function() {
       console.log("Error");
      }
   });
};

$(document).ready(function(){

  $("#boton").click(function(e){

    valores();
    e.preventDefault();
    buscar();
  });
});


function buscar(){
  var texto = $("input#texto").val();
  if(texto.length != 0){
      var tw = $('#tweets .tweet-body');
      $('#tweets .tweet-body').filter(function(index){
        $(this).show()

        var tw = $(this).text()
        if(tw.indexOf(texto) == -1){
            $(this).hide()
        }
      });
    }

  return false;
}
